﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeNA_Prep_and_Finalize
{
    public partial class Generate_TM_TB : Form
    {
        private string tmtbpath;
        public Generate_TM_TB(string tm_tb_path)
        {
            InitializeComponent();
            tmtbpath = tm_tb_path;
        }

        public void Run()
        {
            DeNA_TB_TM_prep_routine();
        }

        private void CloseMsg_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Generate_TM_TB_Load(object sender, EventArgs e)
        {
            TopMost = true;
        }

        public void DeNA_TB_TM_prep_routine()
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            Debug.WriteLine("Copy & paste the folder containing the files to convert");
            string @path = tmtbpath;

            path = path.Replace("\"", "");

            string[] files = Directory.GetFiles(path, "*.xls*", SearchOption.TopDirectoryOnly);

            foreach (string file in files)
            {
                string filename = new FileInfo(file).Name;
                //Debug.WriteLine(filename);
                if (filename.StartsWith("Message_Data_"))
                {

                    //hack: fix the Message_Data_WebviewSemiGlossary_yyyymmdd.xlsx files headers just fix and forget
                    if (filename.StartsWith("Message_Data_WebviewSemiGlossary_"))
                    {
                        ExcelPackage wbFix = new ExcelPackage(new FileInfo(file));

                        foreach (ExcelWorksheet wsFix in wbFix.Workbook.Worksheets)
                        {
                            wsFix.Cells[1, 2].Value = "StringID";
                        }

                        wbFix.Save();
                    }


                    if (Directory.Exists(Path.Combine(path, "DeNA_TM")))
                    {
                        Debug.WriteLine("Saving to the \"Dena_TM\" folder");
                        Debug.WriteLine(filename);

                        richTextConsoleBox1.AppendText("\n------------------\nSaving to the \"Dena_TM\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_DeNA_TM(file, Path.Combine(path, "DeNA_TM"), filename);
                    }
                    else
                    {
                        Debug.WriteLine("Creating \"DeNA_TM\" folder");
                        Directory.CreateDirectory(Path.Combine(path, "DeNA_TM"));
                        Debug.WriteLine("Saving to the \"Dena_TM\" folder");
                        Debug.WriteLine(filename);

                        richTextConsoleBox1.AppendText("\n------------------\nCreating \"DeNA_TM\" folder\n");
                        richTextConsoleBox1.AppendText("Saving to the \"Dena_TM\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_DeNA_TM(file, Path.Combine(path, "DeNA_TM"), filename);
                    }

                }

                if (filename.Contains("Glossary_"))
                {
                    if (Directory.Exists(Path.Combine(path, "For_TB")))
                    {
                        Debug.WriteLine("Saving to the \"For_TB\" folder");
                        Debug.WriteLine(filename);
                        
                        richTextConsoleBox1.AppendText("\n------------------\nSaving to the \"For_TB\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_TB(file, Path.Combine(path, "For_TB"), filename);
                    }
                    else
                    {
                        Debug.WriteLine("Creating \"For_TB\" folder");
                        Directory.CreateDirectory(Path.Combine(path, "For_TB"));
                        Debug.WriteLine("Saving to the \"For_TB\" folder");
                        Debug.WriteLine(filename);

                        richTextConsoleBox1.AppendText("\n------------------\nCreating \"For_TB\" folder\n");
                        richTextConsoleBox1.AppendText("Saving to the \"For_TB\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_TB(file, Path.Combine(path, "For_TB"), filename);
                    }
                }

                if (!filename.StartsWith("Message_Data_") && !filename.Contains("Glossary_"))
                {
                    if (Directory.Exists(Path.Combine(path, "KWS_TM")))
                    {
                        Debug.WriteLine("Saving to the \"KWS_TM\" folder");
                        Debug.WriteLine(filename);

                        richTextConsoleBox1.AppendText("\n------------------\nSaving to the \"KWS_TM\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_KWS_TM(file, Path.Combine(path, "KWS_TM"), filename);
                    }
                    else
                    {
                        Debug.WriteLine("Creating \"KWS_TM\"  folder");
                        Directory.CreateDirectory(Path.Combine(path, "KWS_TM"));
                        Debug.WriteLine("Saving to the \"KWS_TM\" folder");
                        Debug.WriteLine(filename);


                        richTextConsoleBox1.AppendText("\n------------------\nCreating \"KWS_TM\" folder\n");
                        richTextConsoleBox1.AppendText("Saving to the \"KWS_TM\" folder\n");
                        richTextConsoleBox1.AppendText($"{filename}\n\n");

                        Create_KWS_TM(file, Path.Combine(path, "KWS_TM"), filename);
                    }
                }
                Debug.WriteLine("");
            }

            //Create a TB file for just the In-Game project
            string[] updated_files = Directory.GetFiles(Path.Combine(path, "DeNA_TM"), "*.xlsx", SearchOption.TopDirectoryOnly);
            foreach (string file in updated_files)
            {
                string filename = new FileInfo(file).Name;
                if (filename.StartsWith("Message_Data_Master_"))
                {
                    //should check if the TB directory exists

                    ExcelPackage currentWB = new ExcelPackage(new FileInfo(file));
                    ExcelWorksheet currentWS = currentWB.Workbook.Worksheets[0];

                    ExcelPackage tempWB = new ExcelPackage();
                    ExcelWorksheet tempWS = tempWB.Workbook.Worksheets.Add("abnormal_state");
                    int rown = 1;
                    for (int row = 1; row < currentWS.Dimension.End.Row+1; row++)
                    {
                        bool keepme = false;
                        if(row == 1)
                        {
                            keepme = true;
                        }

                        for (int col = 1; col < currentWS.Dimension.End.Column + 1; col++)
                        {

                            if(col == 1)
                            {
                                if(currentWS.Cells[row, col].Value != null)
                                {
                                    if (currentWS.Cells[row, col].Value.ToString() == "abnormal_state")
                                    {
                                        keepme = true;
                                    }
                                }
                            }
                            if(keepme == true)
                            {
                                if (currentWS.Cells[row, col].Value != null)
                                {
                                    tempWS.Cells[rown, col].Value = currentWS.Cells[row, col].Value.ToString();
                                }
                            }
                            
                        }
                        if(keepme == true)
                        {
                            rown++;
                        }
                    }

                    tempWB.SaveAs(new FileInfo(Path.Combine(path, "DeNA_TM",$"INGAME_GLOSSARY_{filename}")));
                    if (Directory.Exists(Path.Combine(path, "For_TB")))
                    {
                        Create_TB(Path.Combine(path, "DeNA_TM", $"INGAME_GLOSSARY_{filename}"), Path.Combine(path, "For_TB"), $"INGAME_GLOSSARY_{filename}");

                    }
                    else
                    {
                        Directory.CreateDirectory(Path.Combine("For_TB"));
                        Create_TB(Path.Combine(path, "DeNA_TM", $"INGAME_GLOSSARY_{filename}"), Path.Combine(path, "For_TB"), $"INGAME_GLOSSARY_{filename}");
                    }

                    File.Delete(Path.Combine(path, "DeNA_TM", $"INGAME_GLOSSARY_{filename}"));
                        
                }
            }

            Debug.WriteLine("Done");
            richTextConsoleBox1.AppendText("\nDONE!!!");


        }

        public void Create_DeNA_TM(string originalfile, string outputpath, string dest_filename)
        {

            string[] version = { "_version", "label", "keyword" };
            string id = "String ID"; // this is "StringID" in the UI master files ... ugn will have to fix this
            string Dena_ID = "StringID";
            string[] jp = { "日本語", "ja", "JPN", "Japanese" };
            string[] en = { "英語", "en", "English" };
            string[] fr = { "フランス語", "fr", "French" };
            string[] it = { "イタリア語", "it", "Italian" };
            string[] de = { "ドイツ語", "de", "German" };
            string[] es = { "スペイン語", "es", "Spanish" };
            string[] ko = { "韓国語", "ko", "Korean" };
            string[] tw = { "繁体字", "zh-TW", "Trad_Chinese", "zh_TW", "Traditional Chinese" };

            ExcelPackage excel_orig = new ExcelPackage(new FileInfo(originalfile));
            ExcelPackage excel_outfile = new ExcelPackage();

            ExcelWorkbook wb_orig = excel_orig.Workbook;
            ExcelWorkbook wb_outfile = excel_outfile.Workbook;

            ExcelWorksheet ws_outfile = wb_outfile.Worksheets.Add("Sheet1");

            int notfound = -1;
            int rowx = 1;

            ws_outfile.Cells[rowx, 1].Value = "Sheet";
            ws_outfile.Cells[rowx, 2].Value = "Row";
            ws_outfile.Cells[rowx, 3].Value = "_Version";
            ws_outfile.Cells[rowx, 4].Value = "String ID";
            ws_outfile.Cells[rowx, 5].Value = "jp";
            ws_outfile.Cells[rowx, 6].Value = "en";
            ws_outfile.Cells[rowx, 7].Value = "fr";
            ws_outfile.Cells[rowx, 8].Value = "it";
            ws_outfile.Cells[rowx, 9].Value = "de";
            ws_outfile.Cells[rowx, 10].Value = "es";
            ws_outfile.Cells[rowx, 11].Value = "ko";
            ws_outfile.Cells[rowx, 12].Value = "zh-TW";
            ws_outfile.Cells[rowx, 13].Value = "DeNa_ID";

            rowx++;

            foreach (ExcelWorksheet ws_infile in wb_orig.Worksheets)
            {
                int _version = notfound;
                int _id = notfound;
                int _jp = notfound;
                int _en = notfound;
                int _fr = notfound;
                int _it = notfound;
                int _de = notfound;
                int _es = notfound;
                int _ko = notfound;
                int _tw = notfound;
                int _denaID = notfound;

                if (ws_infile.Hidden.ToString() == "Visible")
                {

                    if (ws_infile.Dimension != null)
                    {
                        for (int header_col = 1; header_col < ws_infile.Dimension.Columns + 1; header_col++)
                        {
                            if (ws_infile.Cells[1, header_col].Value != null)
                            {
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), version))
                                {
                                    _version = header_col;
                                }
                                if (ws_infile.Cells[1, header_col].Value.ToString() == id)
                                {
                                    _id = header_col;
                                }
                                if (ws_infile.Cells[1, header_col].Value.ToString() == Dena_ID)
                                {
                                    _denaID = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), jp))
                                {
                                    _jp = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), en))
                                {
                                    _en = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), fr))
                                {
                                    _fr = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), it))
                                {
                                    _it = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), de))
                                {
                                    _de = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), es))
                                {
                                    _es = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), ko))
                                {
                                    _ko = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), tw))
                                {
                                    _tw = header_col;
                                }
                            }

                        }


                        Debug.WriteLine(
                            $"Version = {CommonUtils.ColumnIndexToColumnLetter(_version)}, ID = {_id}, DenaID = {_denaID}, JP = {_jp}, EN = {_en}, FR = {_fr}, " +
                            $"DE = {_de}, ES = {_es}, KO = {_ko}, TW = {_tw}\t{ws_infile.Name}"
                            );
                        string msg = 
                            $"Sheet = {ws_infile.Name}\n Version = {CommonUtils.ColumnIndexToColumnLetter(_version)}" +
                            $", ID = {CommonUtils.ColumnIndexToColumnLetter(_id)}" +
                            $", DenaID = {CommonUtils.ColumnIndexToColumnLetter(_denaID)}" +
                            $", JP = {CommonUtils.ColumnIndexToColumnLetter(_jp)}" +
                            $", EN = {CommonUtils.ColumnIndexToColumnLetter(_en)}" +
                            $", FR = {CommonUtils.ColumnIndexToColumnLetter(_fr)}" +
                            $", DE = {CommonUtils.ColumnIndexToColumnLetter(_de)}" +
                            $", ES = {CommonUtils.ColumnIndexToColumnLetter(_es)}" +
                            $", KO = {CommonUtils.ColumnIndexToColumnLetter(_ko)}" +
                            $", TW = {CommonUtils.ColumnIndexToColumnLetter(_tw)}\n\n";

                        richTextConsoleBox1.AppendText(msg);

                        /* Old Code */

                        if (_jp > notfound)
                        {

                            for (int row = 2; row < ws_infile.Dimension.Rows + 1; row++)
                            {
                                //Filter out any segments that are Debug
                                if (ws_infile.Row(row).Hidden == false)
                                {
                                    if (_version > notfound)
                                    {
                                        if (ws_infile.Cells[row, _version].Value != null)
                                        {
                                            if (ws_infile.Cells[row, _version].Style.Fill.BackgroundColor.Rgb == "FF808080")
                                            {
                                                Debug.WriteLine($"{row} :: {ws_infile.Cells[row, _version].Style.Fill.BackgroundColor.Rgb}");
                                            }


                                            //if (ws_infile.Cells[row, _version].Value.ToString().Trim().Contains("debug") == false &&
                                            //ws_infile.Cells[row, _version].Value.ToString().Trim().Contains("denug") == false &&
                                            //ws_infile.Cells[row, _version].Value.ToString().Trim().Contains("未使用") == false &&
                                            //ws_infile.Cells[row, _version].Style.Fill.BackgroundColor.Rgb != "FF808080"
                                            //)
                                            {
                                                //only import relevent stuff if all rows contain nothing don't bother importing
                                                //let this apply only to the TMs 
                                                int all_blanks = 1;

                                                if (_en > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _en].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _en].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }

                                                }

                                                if (_fr > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _fr].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _fr].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }


                                                }

                                                if (_it > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _it].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _it].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }

                                                }

                                                if (_de > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _de].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _de].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }


                                                }
                                                if (_es > notfound)
                                                {

                                                    if (ws_infile.Cells[row, _es].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _es].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }


                                                }
                                                if (_ko > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _ko].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _ko].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }

                                                }
                                                if (_tw > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _tw].Value != null)
                                                    {
                                                        if (ws_infile.Cells[row, _tw].Value.ToString().Trim() != "")
                                                        {
                                                            all_blanks++;
                                                        }
                                                    }

                                                }

                                                if (all_blanks > 0)
                                                {
                                                    ws_outfile.Cells[rowx, 1].Value = ws_infile.Name;
                                                    ws_outfile.Cells[rowx, 2].Value = row;

                                                    if (ws_infile.Cells[row, _version].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 3].Value = ws_infile.Cells[row, _version].Value.ToString().Trim();
                                                    }



                                                    if (_id > notfound)
                                                    {

                                                        if (ws_infile.Cells[row, _id].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 4].Value = ws_infile.Cells[row, _id].Value;
                                                            if (ws_infile.Cells[row, _id].Value != null)
                                                            {
                                                                ws_outfile.Cells[rowx, 13].Value = $"{ws_infile.Name}/{ws_infile.Cells[row, _id].Value.ToString().Trim()}";
                                                            }

                                                        }
                                                        else
                                                        {
                                                            ws_outfile.Cells[rowx, 4].Value = ws_infile.Cells[row, _id].Value;
                                                        }


                                                    }
                                                    if (_denaID > notfound)
                                                    {
                                                        //the UI has this whole thing so no need to reconstruct it ... 
                                                        ws_outfile.Cells[rowx, 13].Value = ws_infile.Cells[row, _denaID].Value;
                                                    }

                                                    if (_jp > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _jp].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 5].Value = CleanString(ws_infile.Cells[row, _jp].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_en > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _en].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 6].Value = CleanString(ws_infile.Cells[row, _en].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_fr > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _fr].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 7].Value = CleanString(ws_infile.Cells[row, _fr].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_it > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _it].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 8].Value = CleanString(ws_infile.Cells[row, _it].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_de > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _de].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 9].Value = CleanString(ws_infile.Cells[row, _de].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_es > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _es].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 10].Value = CleanString(ws_infile.Cells[row, _es].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_ko > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _ko].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 11].Value = CleanString(ws_infile.Cells[row, _ko].Value.ToString().Trim());
                                                        }

                                                    }
                                                    if (_tw > notfound)
                                                    {
                                                        if (ws_infile.Cells[row, _tw].Value != null)
                                                        {
                                                            ws_outfile.Cells[rowx, 12].Value = CleanString(ws_infile.Cells[row, _tw].Value.ToString().Trim());
                                                        }

                                                    }


                                                    rowx++;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            excel_outfile.SaveAs(new FileInfo(Path.Combine(outputpath, dest_filename)));

        }

        public void Create_KWS_TM(string originalfile, string outputpath, string dest_filename)
        {
            string[] version = { "Ver.", "_version" };
            string stringID = "String ID";
            string locID = "label";
            string comments = "種類";
            string[] jp = { "JPN", "ja" };
            string[] en = { "English", "en" };
            string[] fr = { "French", "fr" };
            string[] it = { "Italian", "it" };
            string[] de = { "German", "de" };
            string[] es = { "Spanish", "es" };
            string[] ko = { "Korean", "ko" };
            string[] tw = { "Trad_Chinese", "Traditional Chinese", "zh-TW" };

            ExcelPackage excel_orig = new ExcelPackage(new FileInfo(originalfile));
            ExcelPackage excel_outfile = new ExcelPackage();

            ExcelWorkbook wb_orig = excel_orig.Workbook;
            ExcelWorkbook wb_outfile = excel_outfile.Workbook;

            ExcelWorksheet ws_outfile = wb_outfile.Worksheets.Add("Sheet1");

            int notfound = -1;
            int rowx = 1;

            ws_outfile.Cells[rowx, 1].Value = "_version";
            ws_outfile.Cells[rowx, 2].Value = "String ID";
            ws_outfile.Cells[rowx, 3].Value = "Loc ID";
            ws_outfile.Cells[rowx, 4].Value = "ＭＳコメント";
            ws_outfile.Cells[rowx, 5].Value = "ja";
            ws_outfile.Cells[rowx, 6].Value = "Character_Limit";
            ws_outfile.Cells[rowx, 7].Value = "Comments_DeNA";
            ws_outfile.Cells[rowx, 8].Value = "En_kw";
            ws_outfile.Cells[rowx, 9].Value = "Fr_kw";
            ws_outfile.Cells[rowx, 10].Value = "It_kw";
            ws_outfile.Cells[rowx, 11].Value = "De_kw";
            ws_outfile.Cells[rowx, 12].Value = "Es_kw";
            ws_outfile.Cells[rowx, 13].Value = "Ko_kw";
            ws_outfile.Cells[rowx, 14].Value = "Zh-Tw_kw";

            rowx++;
            foreach (ExcelWorksheet ws_infile in wb_orig.Worksheets)
            {
                int _version = notfound;
                int _stringid = notfound;
                int _locid = notfound;
                int _comments = notfound;
                int _jp = notfound;
                int _en = notfound;
                int _fr = notfound;
                int _it = notfound;
                int _de = notfound;
                int _es = notfound;
                int _ko = notfound;
                int _tw = notfound;


                if (ws_infile.Hidden.ToString() == "Visible")
                {

                    if (ws_infile.Dimension != null)
                    {
                        for (int header_col = 1; header_col < ws_infile.Dimension.Columns + 1; header_col++)
                        {
                            if (ws_infile.Cells[1, header_col].Value != null)
                            {
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), version))
                                {
                                    _version = header_col;
                                }
                                if (ws_infile.Cells[1, header_col].Value.ToString() == stringID)
                                {
                                    _stringid = header_col;
                                }
                                if (ws_infile.Cells[1, header_col].Value.ToString() == locID)
                                {
                                    _locid = header_col;
                                }
                                if (ws_infile.Cells[1, header_col].Value.ToString() == comments)
                                {
                                    _comments = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), jp))
                                {
                                    _jp = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), en))
                                {
                                    _en = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), fr))
                                {
                                    _fr = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), it))
                                {
                                    _it = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), de))
                                {
                                    _de = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), es))
                                {
                                    _es = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), ko))
                                {
                                    _ko = header_col;
                                }
                                if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), tw))
                                {
                                    _tw = header_col;
                                }
                            }

                        }


                        Debug.WriteLine(
                            $"Version = {_version}, String ID = {_stringid}, Loc ID = {_locid}, JP = {_jp}, EN = {_en}, FR = {_fr}, " +
                            $"DE = {_de}, ES = {_es}, KO = {_ko}, TW = {_tw} comments = {_comments}\t{ws_infile.Name}"
                            );
                        string msg = (
                            $"Sheet = {ws_infile.Name}\nVersion = {CommonUtils.ColumnIndexToColumnLetter(_version)}" +
                            $", String ID = {CommonUtils.ColumnIndexToColumnLetter(_stringid)}" +
                            $", Loc ID = {CommonUtils.ColumnIndexToColumnLetter(_locid)}" +
                            $", JP = {CommonUtils.ColumnIndexToColumnLetter(_jp)}" +
                            $", EN = {CommonUtils.ColumnIndexToColumnLetter(_en)}" +
                            $", FR = {CommonUtils.ColumnIndexToColumnLetter(_fr)}" +
                            $", DE = {CommonUtils.ColumnIndexToColumnLetter(_de)}" +
                            $", ES = {CommonUtils.ColumnIndexToColumnLetter(_es)}" +
                            $", KO = {CommonUtils.ColumnIndexToColumnLetter(_ko)}" +
                            $", TW = {CommonUtils.ColumnIndexToColumnLetter(_tw)}" +
                            $" comments = {CommonUtils.ColumnIndexToColumnLetter(_comments)}\n\n"
                            );
                        richTextConsoleBox1.AppendText(msg);

                    }



                    for (int row = 2; row < ws_infile.Dimension.Rows + 1; row++)
                    {
                        //Filter out any segments that are Debug
                        if (ws_infile.Row(row).Hidden == false)
                        {
                            if (_comments > notfound)
                            {
                                if (ws_infile.Cells[row, _comments].Value != null)
                                {
                                    if (ws_infile.Cells[row, _comments].Style.Fill.BackgroundColor.Rgb == "FF808080")
                                    {
                                        Debug.WriteLine($"{row} :: {ws_infile.Cells[row, _comments].Style.Fill.BackgroundColor.Rgb}");
                                    }


                                    if (ws_infile.Cells[row, _comments].Value.ToString().Trim().Contains("未使用") == false)
                                    {
                                        //only import relevent stuff if all rows contain nothing don't bother importing
                                        //let this apply only to the TMs 
                                        int all_blanks = 1;

                                        if (_en > notfound)
                                        {
                                            if (ws_infile.Cells[row, _en].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _en].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }

                                        }

                                        if (_fr > notfound)
                                        {
                                            if (ws_infile.Cells[row, _fr].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _fr].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }


                                        }

                                        if (_it > notfound)
                                        {
                                            if (ws_infile.Cells[row, _it].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _it].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }

                                        }

                                        if (_de > notfound)
                                        {
                                            if (ws_infile.Cells[row, _de].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _de].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }


                                        }
                                        if (_es > notfound)
                                        {

                                            if (ws_infile.Cells[row, _es].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _es].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }


                                        }
                                        if (_ko > notfound)
                                        {
                                            if (ws_infile.Cells[row, _ko].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _ko].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }

                                        }
                                        if (_tw > notfound)
                                        {
                                            if (ws_infile.Cells[row, _tw].Value != null)
                                            {
                                                if (ws_infile.Cells[row, _tw].Value.ToString().Trim() != "")
                                                {
                                                    all_blanks++;
                                                }
                                            }

                                        }

                                        if (all_blanks > 0)
                                        {


                                            if (ws_infile.Cells[row, _version].Value != null)
                                            {
                                                ws_outfile.Cells[rowx, 1].Value = ws_infile.Cells[row, _version].Value.ToString().Trim();
                                            }

                                            if (ws_infile.Cells[row, _comments].Value != null)
                                            {
                                                ws_outfile.Cells[rowx, 7].Value = ws_infile.Cells[row, _comments].Value.ToString().Trim();
                                            }


                                            if (_stringid > notfound)
                                            {

                                                if (ws_infile.Cells[row, _stringid].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 2].Value = ws_infile.Cells[row, _stringid].Value;
                                                }

                                            }
                                            if (_locid > notfound)
                                            {
                                                //the UI has this whole thing so no need to reconstruct it ... 
                                                ws_outfile.Cells[rowx, 3].Value = ws_infile.Cells[row, _locid].Value;
                                            }

                                            if (_jp > notfound)
                                            {
                                                if (ws_infile.Cells[row, _jp].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 5].Value = CleanString(ws_infile.Cells[row, _jp].Value.ToString().Trim());
                                                }

                                            }
                                            if (_en > notfound)
                                            {
                                                if (ws_infile.Cells[row, _en].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 8].Value = CleanString(ws_infile.Cells[row, _en].Value.ToString().Trim());
                                                }

                                            }
                                            if (_fr > notfound)
                                            {
                                                if (ws_infile.Cells[row, _fr].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 9].Value = CleanString(ws_infile.Cells[row, _fr].Value.ToString().Trim());
                                                }

                                            }
                                            if (_it > notfound)
                                            {
                                                if (ws_infile.Cells[row, _it].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 10].Value = CleanString(ws_infile.Cells[row, _it].Value.ToString().Trim());
                                                }

                                            }
                                            if (_de > notfound)
                                            {
                                                if (ws_infile.Cells[row, _de].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 11].Value = CleanString(ws_infile.Cells[row, _de].Value.ToString().Trim());
                                                }

                                            }
                                            if (_es > notfound)
                                            {
                                                if (ws_infile.Cells[row, _es].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 12].Value = CleanString(ws_infile.Cells[row, _es].Value.ToString().Trim());
                                                }

                                            }
                                            if (_ko > notfound)
                                            {
                                                if (ws_infile.Cells[row, _ko].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 13].Value = CleanString(ws_infile.Cells[row, _ko].Value.ToString().Trim());
                                                }

                                            }
                                            if (_tw > notfound)
                                            {
                                                if (ws_infile.Cells[row, _tw].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 14].Value = CleanString(ws_infile.Cells[row, _tw].Value.ToString().Trim());
                                                }

                                            }
                                            rowx++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //if the comment row is blank we may need to process this in the future, but haven't seen this case
                                //Dena Changed their format 8/21/2020
                                if (_version > notfound)
                                {
                                    if (ws_infile.Cells[row, _version].Value != null)
                                    {
                                        if (ws_infile.Cells[row, _version].Value.ToString().Trim().Contains("debug") == false)
                                        {
                                            //only import relevent stuff if all rows contain nothing don't bother importing
                                            //let this apply only to the TMs 
                                            int all_blanks = 0;

                                            if (_en > notfound)
                                            {
                                                if (ws_infile.Cells[row, _en].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _en].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }

                                            }

                                            if (_fr > notfound)
                                            {
                                                if (ws_infile.Cells[row, _fr].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _fr].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }


                                            }

                                            if (_it > notfound)
                                            {
                                                if (ws_infile.Cells[row, _it].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _it].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }

                                            }

                                            if (_de > notfound)
                                            {
                                                if (ws_infile.Cells[row, _de].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _de].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }


                                            }
                                            if (_es > notfound)
                                            {

                                                if (ws_infile.Cells[row, _es].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _es].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }


                                            }
                                            if (_ko > notfound)
                                            {
                                                if (ws_infile.Cells[row, _ko].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _ko].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }

                                            }
                                            if (_tw > notfound)
                                            {
                                                if (ws_infile.Cells[row, _tw].Value != null)
                                                {
                                                    if (ws_infile.Cells[row, _tw].Value.ToString().Trim() != "")
                                                    {
                                                        all_blanks++;
                                                    }
                                                }

                                            }

                                            if (all_blanks > 0)
                                            {


                                                if (ws_infile.Cells[row, _version].Value != null)
                                                {
                                                    ws_outfile.Cells[rowx, 1].Value = ws_infile.Cells[row, _version].Value.ToString().Trim();
                                                }

                                                if (_comments > notfound)
                                                {

                                                    if (ws_infile.Cells[row, _comments].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 7].Value = ws_infile.Cells[row, _comments].Value.ToString().Trim();
                                                    }
                                                }

                                                if (_stringid > notfound)
                                                {

                                                    if (ws_infile.Cells[row, _stringid].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 2].Value = ws_infile.Cells[row, _stringid].Value;
                                                    }

                                                }
                                                if (_locid > notfound)
                                                {
                                                    //the UI has this whole thing so no need to reconstruct it ... 
                                                    ws_outfile.Cells[rowx, 3].Value = ws_infile.Cells[row, _locid].Value;
                                                }

                                                if (_jp > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _jp].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 5].Value = CleanString(ws_infile.Cells[row, _jp].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_en > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _en].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 8].Value = CleanString(ws_infile.Cells[row, _en].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_fr > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _fr].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 9].Value = CleanString(ws_infile.Cells[row, _fr].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_it > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _it].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 10].Value = CleanString(ws_infile.Cells[row, _it].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_de > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _de].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 11].Value = CleanString(ws_infile.Cells[row, _de].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_es > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _es].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 12].Value = CleanString(ws_infile.Cells[row, _es].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_ko > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _ko].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 13].Value = CleanString(ws_infile.Cells[row, _ko].Value.ToString().Trim());
                                                    }

                                                }
                                                if (_tw > notfound)
                                                {
                                                    if (ws_infile.Cells[row, _tw].Value != null)
                                                    {
                                                        ws_outfile.Cells[rowx, 14].Value = CleanString(ws_infile.Cells[row, _tw].Value.ToString().Trim());
                                                    }

                                                }
                                                rowx++;
                                            }
                                        }

                                    }

                                }




                            }

                        }
                    }

                }
            }

            excel_outfile.SaveAs(new FileInfo(Path.Combine(outputpath, dest_filename)));

        }

        public void Create_TB(string originalfile, string outputpath, string dest_filename)
        {
            /* Our Dictionary of headers */
            string[] version = { "_version", "label", "keyword", "種類" };
            string id = "String ID"; // this is "StringID" in the UI master files ... ugn will have to fix this
            string Dena_ID = "StringID";
            string[] jp = { "日本語", "ja","jp", "JPN", "Japanese" };
            string[] en = { "英語", "en", "English" };
            string[] fr = { "フランス語", "fr", "French" };
            string[] it = { "イタリア語", "it", "Italian" };
            string[] de = { "ドイツ語", "de", "German" };
            string[] es = { "スペイン語", "es", "Spanish" };
            string[] ko = { "韓国語", "ko", "Korean" };
            string[] tw = { "繁体字", "zh-TW", "Trad_Chinese", "zh_TW", "Traditional Chinese" };

            int notfound = -1;

            int rowx = 1;


            string file = originalfile;

            if (originalfile.EndsWith(".xls"))
            {
                file = originalfile.Replace(".xls", ".xlsx");

                byte[] data_in = File.ReadAllBytes(originalfile);
                byte[] data_out;

                data_out = XLSToXLSXConverter.Convert(new MemoryStream(data_in));

                File.WriteAllBytes(file, data_out);

            }





            ExcelPackage excel_infile = new ExcelPackage(new FileInfo(file));
            ExcelWorkbook wb_infile = excel_infile.Workbook;

            ExcelPackage excel_outfile = new ExcelPackage();
            ExcelWorkbook wb_outfile = excel_outfile.Workbook;
            ExcelWorksheet ws_outfile = wb_outfile.Worksheets.Add("Sheet1");


            ws_outfile.Cells[rowx, 1].Value = "Entry_ID";
            //ws_outfile.Cells[rowx, 1].Value = "Entry_Subject";
            //ws_outfile.Cells[rowx, 2].Value = "Entry_Domain";
            //ws_outfile.Cells[rowx, 3].Value = "Entry_ClientID";
            //ws_outfile.Cells[rowx, 4].Value = "Entry_ProjectID";
            //ws_outfile.Cells[rowx, 5].Value = "Entry_Created";
            //ws_outfile.Cells[rowx, 6].Value = "Entry_Creator";
            //ws_outfile.Cells[rowx, 7].Value = "Entry_LastModified";
            //ws_outfile.Cells[rowx, 8].Value = "Entry_Modifier";
            ws_outfile.Cells[rowx, 10].Value = "Entry_Note";

            ws_outfile.Cells[rowx, 11].Value = "English_Def";
            ws_outfile.Cells[rowx, 12].Value = "English";
            ws_outfile.Cells[rowx, 13].Value = "Term_Info";
            ws_outfile.Cells[rowx, 14].Value = "Term_Example";

            ws_outfile.Cells[rowx, 15].Value = "French_Def";
            ws_outfile.Cells[rowx, 16].Value = "French";
            ws_outfile.Cells[rowx, 17].Value = "Term_Info";
            ws_outfile.Cells[rowx, 18].Value = "Term_Example";

            ws_outfile.Cells[rowx, 19].Value = "German_Def";
            ws_outfile.Cells[rowx, 20].Value = "German";
            ws_outfile.Cells[rowx, 21].Value = "Term_Info";
            ws_outfile.Cells[rowx, 22].Value = "Term_Example";

            ws_outfile.Cells[rowx, 23].Value = "Italian_Def";
            ws_outfile.Cells[rowx, 24].Value = "Italian";
            ws_outfile.Cells[rowx, 25].Value = "Term_Info";
            ws_outfile.Cells[rowx, 26].Value = "Term_Example";

            ws_outfile.Cells[rowx, 27].Value = "Japanese_Def";
            ws_outfile.Cells[rowx, 28].Value = "Japanese";
            ws_outfile.Cells[rowx, 29].Value = "Term_Info";
            ws_outfile.Cells[rowx, 30].Value = "Term_Example";

            ws_outfile.Cells[rowx, 31].Value = "Korean_Def";
            ws_outfile.Cells[rowx, 32].Value = "Korean";
            ws_outfile.Cells[rowx, 33].Value = "Term_Info";
            ws_outfile.Cells[rowx, 34].Value = "Term_Example";

            ws_outfile.Cells[rowx, 35].Value = "Spanish_Def";
            ws_outfile.Cells[rowx, 36].Value = "Spanish";
            ws_outfile.Cells[rowx, 37].Value = "Term_Info";
            ws_outfile.Cells[rowx, 38].Value = "Term_Example";

            ws_outfile.Cells[rowx, 39].Value = "Chinese_Taiwan_Def";
            ws_outfile.Cells[rowx, 40].Value = "Chinese_Taiwan";
            ws_outfile.Cells[rowx, 41].Value = "Term_Info";
            ws_outfile.Cells[rowx, 42].Value = "Term_Example";

            rowx++;

            foreach (ExcelWorksheet ws_infile in wb_infile.Worksheets)
            {
                int _version = notfound;
                int _id = notfound;
                int _jp = notfound;
                int _en = notfound;
                int _fr = notfound;
                int _it = notfound;
                int _de = notfound;
                int _es = notfound;
                int _ko = notfound;
                int _tw = notfound;
                int _denaID = notfound;

                if (ws_infile.Hidden.ToString() == "Visible")
                {
                    for (int header_col = 1; header_col < ws_infile.Dimension.Columns + 1; header_col++)
                    {
                        if (ws_infile.Cells[1, header_col].Value != null)
                        {
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), version))
                            {
                                _version = header_col;
                            }
                            if (ws_infile.Cells[1, header_col].Value.ToString() == id)
                            {
                                _id = header_col;
                            }
                            if (ws_infile.Cells[1, header_col].Value.ToString() == Dena_ID)
                            {
                                _denaID = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), jp))
                            {
                                _jp = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), en))
                            {
                                _en = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), fr))
                            {
                                _fr = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), it))
                            {
                                _it = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), de))
                            {
                                _de = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), es))
                            {
                                _es = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), ko))
                            {
                                _ko = header_col;
                            }
                            if (LanguageFound(ws_infile.Cells[1, header_col].Value.ToString(), tw))
                            {
                                _tw = header_col;
                            }

                        }

                    }


                    Debug.WriteLine(
                        $"Version = {_version}, ID = {_id}, DenaID = {_denaID}, JP = {_jp}, EN = {_en}, FR = {_fr}, " +
                        $"DE = {_de}, ES = {_es}, KO = {_ko}, TW = {_tw}\t{ws_infile.Name}"
                        );
                    string msg = 
                        $"Sheet = {ws_infile.Name}\nVersion = {CommonUtils.ColumnIndexToColumnLetter(_version)}" +
                        $", ID = {CommonUtils.ColumnIndexToColumnLetter(_id)}" +
                        $", DenaID = {CommonUtils.ColumnIndexToColumnLetter(_denaID)}" +
                        $", JP = {CommonUtils.ColumnIndexToColumnLetter(_jp)}" +
                        $", EN = {CommonUtils.ColumnIndexToColumnLetter(_en)}" +
                        $", FR = {CommonUtils.ColumnIndexToColumnLetter(_fr)}" +
                        $", DE = {CommonUtils.ColumnIndexToColumnLetter(_de)}" +
                        $", ES = {CommonUtils.ColumnIndexToColumnLetter(_es)}" +
                        $", KO = {CommonUtils.ColumnIndexToColumnLetter(_ko)}" +
                        $", TW = {CommonUtils.ColumnIndexToColumnLetter(_tw)}\n\n";
                    richTextConsoleBox1.AppendText(msg);
                }

                for (int row = 2; row < ws_infile.Dimension.Rows + 1; row++)
                {
                    //Filter out any segments that are Debug
                    if (ws_infile.Row(row).Hidden == false)
                    {


                        bool case_insensitive = true;
                        if (ws_infile.Name == "パッシブスキル")
                        {
                            case_insensitive = false;
                        }
                        if (_id > notfound)
                        {
                            //ws_outfile.Cells[rowx, 3].Value = CleanString(ws_infile.Cells[row, _id].StringValue.Trim());
                        }
                        if (_jp > notfound)
                        {
                            if (ws_infile.Cells[row, _jp].Value != null)
                            {
                                ws_outfile.Cells[rowx, 1].Value = rowx - 2;
                                ws_outfile.Cells[rowx, 10].Value = ws_infile.Name;

                                ws_outfile.Cells[rowx, 28].Value = CleanString(ws_infile.Cells[row, _jp].Value.ToString().Trim());
                                if (case_insensitive == false)
                                {
                                    ws_outfile.Cells[rowx, 29].Value = "CasePermissive;HalfPrefix";
                                }
                                else
                                {
                                    ws_outfile.Cells[rowx, 29].Value = "CaseInsense;HalfPrefix";
                                }

                                //move the logic to write only if we have something in jp column

                                if (_en > notfound)
                                {
                                    if (ws_infile.Cells[row, _en].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 12].Value = CleanString(ws_infile.Cells[row, _en].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 13].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 13].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_fr > notfound)
                                {
                                    if (ws_infile.Cells[row, _fr].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 16].Value = CleanString(ws_infile.Cells[row, _fr].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 17].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 17].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_it > notfound)
                                {
                                    if (ws_infile.Cells[row, _it].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 24].Value = CleanString(ws_infile.Cells[row, _it].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 25].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 25].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_de > notfound)
                                {
                                    if (ws_infile.Cells[row, _de].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 20].Value = CleanString(ws_infile.Cells[row, _de].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 21].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 21].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_es > notfound)
                                {
                                    if (ws_infile.Cells[row, _es].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 36].Value = CleanString(ws_infile.Cells[row, _es].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 37].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 37].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_ko > notfound)
                                {
                                    if (ws_infile.Cells[row, _ko].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 32].Value = CleanString(ws_infile.Cells[row, _ko].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 33].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 33].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                if (_tw > notfound)
                                {
                                    if (ws_infile.Cells[row, _tw].Value != null)
                                    {
                                        ws_outfile.Cells[rowx, 40].Value = CleanString(ws_infile.Cells[row, _tw].Value.ToString().Trim());
                                        if (case_insensitive == false)
                                        {
                                            ws_outfile.Cells[rowx, 41].Value = "CasePermissive;HalfPrefix";
                                        }
                                        else
                                        {
                                            ws_outfile.Cells[rowx, 41].Value = "CaseInsense;HalfPrefix";
                                        }
                                    }

                                }
                                rowx++;


                            }
                            else
                            {
                                //if the JP is empty we aren't even going to write the rest of the rows



                            }


                        }


                    }
                }



            }

            if (originalfile.EndsWith(".xls"))
            {

                dest_filename = dest_filename.Replace(".xls", ".xlsx");
            }

            excel_outfile.SaveAs(new FileInfo(Path.Combine(outputpath, dest_filename)));

        }

        static bool LanguageFound(string HeaderKey, string[] languageVarients)
        {
            bool found = false;
            foreach (string language in languageVarients)
            {
                if (HeaderKey == language)
                {
                    found = true;
                }
            }

            return found;
        }

        static string CleanString(string original_string)
        {
            string cleaned_segment = Regex.Replace(original_string, "_?x000D_?", "");
            cleaned_segment = Regex.Replace(cleaned_segment, "\r", "");
            return cleaned_segment;
        }

        private void ConsoleMsg_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextConsoleBox1_TextChanged(object sender, EventArgs e)
        {
            richTextConsoleBox1.SelectionStart = richTextConsoleBox1.Text.Length;
            richTextConsoleBox1.ScrollToCaret();
        }
    }
}
