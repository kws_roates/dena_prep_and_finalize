﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DeNA_Prep_and_Finalize
{
    public partial class IDtoReference : Form
    {
        private string Ref_Folder_Path;
        private string Mqxliff_Path;
        private string src_trg_choice;

        public IDtoReference(string Ref_Folder_Path, string Mqxliff_Path, string src_trg_choice)
        {
            InitializeComponent();
            this.Ref_Folder_Path = Ref_Folder_Path;
            this.Mqxliff_Path = Mqxliff_Path;
            this.src_trg_choice = src_trg_choice;
        }

        public void Run()
        {
            IDtoReference_routine();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void IDtoReference_routine()
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
          
            string message_data_path = @Ref_Folder_Path;
            string mqxliff_path = @Mqxliff_Path;

            string[] message_data_files = Directory.GetFiles(message_data_path, "*.xlsx", SearchOption.TopDirectoryOnly);

            List<Message_Data> message_data = new List<Message_Data>();

            string pattern_get_id_and_reference = "(ID_.*?)=([0-9a-zA-Z_\\/\\{\\}#\\.\\-]+)";
            string pattern_get_reference = "(?<=\\{\\{.*?}\\})(.*?)(?=\\{\\{.*?}\\})";
            string pattern_get_reference_directly = "displaytext=\"(\\{\\{#_\\}\\})(.*?)(\\{\\{\\/_\\}\\})\"";

            foreach (string message_data_file in message_data_files)
            {
                //make this flexible later
                ExcelPackage wb = new ExcelPackage(new FileInfo(message_data_file));
                ExcelWorksheet ws = wb.Workbook.Worksheets["Sheet1"];

                for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                {
                    if (ws.Cells[row, 13].Value != null)
                    {
                        
                        string jp = string.Empty;
                        if(ws.Cells[row, 5].Value != null)
                        {
                            jp = ws.Cells[row, 5].Value.ToString();
                        }

                        string en = string.Empty;
                        if (ws.Cells[row, 6].Value!=null)
                        {
                            en = ws.Cells[row, 6].Value.ToString();
                        }

                        string fr = string.Empty;
                        if (ws.Cells[row, 7].Value!=null)
                        {
                            fr = ws.Cells[row, 7].Value.ToString();
                        }

                        string it = string.Empty;
                        if (ws.Cells[row, 8].Value!=null)
                        {
                            it = ws.Cells[row, 8].Value.ToString();
                        }


                        string de = string.Empty;
                        if (ws.Cells[row, 9].Value!=null)
                        {
                            de = ws.Cells[row, 9].Value.ToString();
                        }


                        string es = string.Empty;
                        if (ws.Cells[row, 10].Value!=null)
                        {
                            es = ws.Cells[row, 10].Value.ToString();
                        }

                        string kr = string.Empty;
                        if (ws.Cells[row, 11].Value!=null)
                        {
                            kr = ws.Cells[row, 11].Value.ToString();
                        }

                        string tw = string.Empty;
                        if (ws.Cells[row, 12].Value!=null)
                        {
                            tw = ws.Cells[row, 12].Value.ToString();
                        }

                        string id = string.Empty;
                        if (ws.Cells[row, 13].Value!=null)
                        {
                            id = ws.Cells[row, 13].Value.ToString();
                        }


                        if (message_data_file.Contains("UI"))
                        {
                            //Debug.WriteLine($"Ref_ID = {id}\nValue = {jp}");
                        }
                        message_data.Add(new Message_Data(id, jp, en, fr, it, de, es, kr, tw));
                    }

                }

            }

            string[] mqxliff_files = Directory.GetFiles(mqxliff_path, "*.mqxliff", SearchOption.TopDirectoryOnly);

            foreach (string mqxliff in mqxliff_files)
            {
                XmlDocument xliff = new XmlDocument();
                List<TransUnit> tu = new List<TransUnit>();

                if (mqxliff.EndsWith("_fre.mqxliff"))
                {
                    Debug.WriteLine("French");
                    richTextConsoleBox1.AppendText("Processing French\n\n");
                }
                if (mqxliff.EndsWith("_ger.mqxliff"))
                {
                    Debug.WriteLine("German");
                    richTextConsoleBox1.AppendText("Processing German\n\n");
                }
                if (mqxliff.EndsWith("_ita.mqxliff"))
                {
                    Debug.WriteLine("Italian");
                    richTextConsoleBox1.AppendText("Processing Italian\n\n");
                }
                if (mqxliff.EndsWith("_spa.mqxliff"))
                {
                    Debug.WriteLine("Spanish");
                    richTextConsoleBox1.AppendText("Processing Spanish\n\n");
                }

                xliff.Load(mqxliff);

                XmlNodeList trans_units = xliff.GetElementsByTagName("trans-unit");

                //read all of the nodes here
                for (int i = 0; i < trans_units.Count; i++)
                {

                    string src = "";
                    string note = "";
                    for (int j = 0; j < trans_units[i].ChildNodes.Count; j++)
                    {


                        if (trans_units[i].ChildNodes[j].LocalName == src_trg_choice)
                        {
                            src = trans_units[i].ChildNodes[j].InnerXml;
                        }

                        if (trans_units[i].ChildNodes[j].LocalName == "note")
                        {

                            note = trans_units[i].ChildNodes[j].InnerText;
                            note = note.Replace("(", "\\(").Replace(")", "\\)");
                        }

                        if (j == trans_units[i].ChildNodes.Count - 1)
                        {
                            tu.Add(new TransUnit(i, src, note));

                        }

                    }

                }


                foreach (var tu_id in tu)
                {
                    //Debug.WriteLine($"{item.Id} :: {item.Note} :: {item.Src}");
                    //Debug.WriteLine($"{tu_id.Id} :: {tu_id.Note}");

                    // get the references from the notes section if provided
                    MatchCollection Id_Reference = Regex.Matches(tu_id.Note, pattern_get_id_and_reference);
                    foreach (Match match in Id_Reference)
                    {
                        string gID = "";
                        string rValue = "";
                        string successful = "";

                        for (int n = 0; n < match.Groups.Count; n++)
                        {
                            if (n == 1)
                            {
                                //ID
                                gID = match.Groups[n].Value;
                            }
                            if (n == 2)
                            {
                                //Reference
                                Match Reference_from_Id = Regex.Match(match.Groups[n].Value, pattern_get_reference);
                                if (Reference_from_Id.Success)
                                {
                                    //Debug.WriteLine($"Success {Reference_from_Id.Value}");
                                    successful = Reference_from_Id.Value;

                                    foreach (var mdata in message_data.Where(x => x.Id == Reference_from_Id.Value))
                                    {

                                        if (mqxliff.EndsWith("_eng.mqxliff"))
                                        {
                                            if (mdata.En == null)
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                if (mdata.En.Trim() == "")
                                                {
                                                    rValue = mdata.Jp;
                                                }
                                                else
                                                {
                                                    rValue = mdata.En;
                                                }

                                            }

                                        }


                                        if (mqxliff.EndsWith("_fre.mqxliff"))
                                        {
                                            if (mdata.Fr == null)
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                if (mdata.Fr.Trim() == "")
                                                {
                                                    rValue = mdata.En;
                                                }
                                                else
                                                {
                                                    rValue = mdata.Fr;
                                                }
                                            }

                                        }
                                        if (mqxliff.EndsWith("_ger.mqxliff"))
                                        {

                                            if (mdata.De == null)
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                if (mdata.De.Trim() == "")
                                                {
                                                    rValue = mdata.En;
                                                }
                                                else
                                                {
                                                    rValue = mdata.De;
                                                }
                                            }

                                        }
                                        if (mqxliff.EndsWith("_ita.mqxliff"))
                                        {

                                            if (mdata.It == null)
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                if (mdata.It.Trim() == "")
                                                {
                                                    rValue = mdata.En;
                                                }
                                                else
                                                {
                                                    rValue = mdata.It;
                                                }
                                            }

                                        }
                                        if (mqxliff.EndsWith("_spa.mqxliff"))
                                        {

                                            if (mdata.Es == null)
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                if (mdata.Es.Trim() == "")
                                                {
                                                    rValue = mdata.En;
                                                }
                                                else
                                                {
                                                    rValue = mdata.Es;
                                                }

                                            }


                                        }
                                        //for KR use JP fallback

                                        if (mqxliff.EndsWith("_kor.mqxliff"))
                                        {
                                            //Debug.WriteLine($"TU: {tu_id.Id} id = {mdata.Id}\nJp = {mdata.Jp}\nEn = {mdata.En}\nFr = {mdata.Fr}");
                                            if (mdata.Kr == null)
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                if (mdata.Kr.Trim() == "")
                                                {
                                                    rValue = mdata.Jp;
                                                }
                                                else
                                                {
                                                    rValue = mdata.Kr;
                                                }

                                            }

                                        }
                                        if (mqxliff.EndsWith("_zho-TW.mqxliff"))
                                        {
                                            //Debug.WriteLine($"TU: {tu_id.Id} id = {mdata.Id}\nJp = {mdata.Jp}\nEn = {mdata.En}\nTW = {mdata.Tw}");
                                            if (mdata.Tw == null)
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                if (mdata.Tw.Trim() == "")
                                                {
                                                    rValue = mdata.Jp;
                                                }
                                                else
                                                {
                                                    rValue = mdata.Tw;
                                                }

                                            }

                                        }
                                        //for TW use JP fallback
                                        //for EN use JP fallback


                                    }

                                }
                                else
                                {
                                    //Debug.WriteLine($"Failed will put this in {match.Groups[n].Value}");
                                    rValue = match.Groups[n].Value;
                                }

                            }

                        }


                        //Debug.WriteLine($"{tu_id.Id} :: gid = {gID} :: rValue = {rValue.Replace("\n"," ")}");
                        //Super Fail safe ... if there is no value for jp!
                        if (rValue == null || rValue == "")
                        {
                            //Debug.WriteLine($"\"{successful}\" does not exist will not update for \"{gID}\" ...");
                            rValue = gID;
                        }
                        //update the stuff in the trans_unit
                        //trans_units[tu_id.Id]

                        for (int j = 0; j < trans_units[tu_id.Id].ChildNodes.Count; j++)
                        {

                            if (trans_units[tu_id.Id].ChildNodes[j].LocalName == src_trg_choice)
                            {
                                //Debug.WriteLine(trans_units[tu_id.Id].ChildNodes[j].ChildNodes.Count);

                                for (int k = 0; k < trans_units[tu_id.Id].ChildNodes[j].ChildNodes.Count; k++)
                                {
                                    string xml_current = trans_units[tu_id.Id].ChildNodes[j].ChildNodes[k].InnerText;
                                    //Debug.WriteLine(xml_current);
                                    //xml_current = xml_current.Replace("(", "{");
                                    //xml_current = xml_current.Replace(")", "}");
                                    string xml_updated = Regex.Replace(xml_current, $"displaytext=\"({gID})\"", $"displaytext=\"{rValue.Replace("\n", " ")}\"");
                                    //Debug.WriteLine(rValue.Replace("\n", " "));
                                    //Debug.WriteLine(xml_updated);
                                    if (xml_current != xml_updated)
                                    {
                                        //Debug.WriteLine($"(current) {xml_current}");
                                        //Debug.WriteLine($"(updated) {xml_updated}");
                                        richTextConsoleBox1.AppendText($"Original:\n {xml_current}\nUpdated:\n{xml_updated}\n\n");
                                        trans_units[tu_id.Id].ChildNodes[j].ChildNodes[k].InnerText = xml_updated;
                                    }

                                }

                            }

                        }

                    } //end of foreach


                    // get the references from the tag directly
                    MatchCollection Id_Direct_References = Regex.Matches(tu_id.Src, pattern_get_reference_directly);
                    foreach (Match Id_Direct_Reference in Id_Direct_References)
                    {
                        string gID = "";
                        string rValue = "";


                        //find the reference directly
                        for (int i = 0; i < Id_Direct_Reference.Groups.Count; i++)
                        {
                            if (i == 2)
                            {
                                gID = Id_Direct_Reference.Groups[i].Value;
                                //Debug.WriteLine($"{gID}");

                                foreach (var mdata in message_data.Where(x => x.Id == gID))
                                {
                                    if (mqxliff.EndsWith("_eng.mqxliff"))
                                    {
                                        //Debug.WriteLine($"TU: {tu_id.Id} id = {mdata.Id}\nJp = {mdata.Jp}\nEn = {mdata.En}\nEn = {mdata.En}");
                                        if (mdata.En == null)
                                        {
                                            rValue = mdata.Jp;
                                        }
                                        else
                                        {
                                            if (mdata.En.Trim() == "")
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                rValue = mdata.En;
                                            }

                                        }

                                    }


                                    if (mqxliff.EndsWith("_fre.mqxliff"))
                                    {
                                        if (mdata.Fr == null)
                                        {
                                            rValue = mdata.En;
                                        }
                                        else
                                        {
                                            if (mdata.Fr.Trim() == "")
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                rValue = mdata.Fr;
                                            }
                                        }

                                    }
                                    if (mqxliff.EndsWith("_ger.mqxliff"))
                                    {

                                        if (mdata.De == null)
                                        {
                                            rValue = mdata.En;
                                        }
                                        else
                                        {
                                            if (mdata.De.Trim() == "")
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                rValue = mdata.De;
                                            }
                                        }

                                    }
                                    if (mqxliff.EndsWith("_ita.mqxliff"))
                                    {

                                        if (mdata.It == null)
                                        {
                                            rValue = mdata.En;
                                        }
                                        else
                                        {
                                            if (mdata.It.Trim() == "")
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                rValue = mdata.It;
                                            }
                                        }

                                    }
                                    if (mqxliff.EndsWith("_spa.mqxliff"))
                                    {

                                        if (mdata.Es == null)
                                        {
                                            rValue = mdata.En;
                                        }
                                        else
                                        {
                                            if (mdata.Es.Trim() == "")
                                            {
                                                rValue = mdata.En;
                                            }
                                            else
                                            {
                                                rValue = mdata.Es;
                                            }

                                        }


                                    }
                                    //for KR use JP fallback

                                    if (mqxliff.EndsWith("_kor.mqxliff"))
                                    {
                                        //Debug.WriteLine($"TU: {tu_id.Id} id = {mdata.Id}\nJp = {mdata.Jp}\nEn = {mdata.En}\nKr = {mdata.Kr}");
                                        if (mdata.Kr == null)
                                        {
                                            rValue = mdata.Jp;
                                        }
                                        else
                                        {
                                            if (mdata.Kr.Trim() == "")
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                rValue = mdata.Kr;
                                            }

                                        }

                                    }
                                    if (mqxliff.EndsWith("_zho-TW.mqxliff"))
                                    {
                                        //Debug.WriteLine($"TU: {tu_id.Id} id = {mdata.Id}\nJp = {mdata.Jp}\nEn = {mdata.En}\nTW = {mdata.Tw}");
                                        if (mdata.Tw == null)
                                        {
                                            rValue = mdata.Jp;
                                        }
                                        else
                                        {
                                            if (mdata.Tw.Trim() == "")
                                            {
                                                rValue = mdata.Jp;
                                            }
                                            else
                                            {
                                                rValue = mdata.Tw;
                                            }

                                        }

                                    }

                                }
                            }

                            if (rValue == null || rValue == "")
                            {
                                //Debug.WriteLine($"\"{successful}\" does not exist will not update for \"{gID}\" ...");
                                rValue = "{{#_}}" + Id_Direct_Reference.Groups[2].Value + "{{/_}}";
                            }
                            //update the xml here
                            //Debug.WriteLine($"Updating src at node {tu_id.Id} {rValue}");

                            for (int j = 0; j < trans_units[tu_id.Id].ChildNodes.Count; j++)
                            {

                                if (trans_units[tu_id.Id].ChildNodes[j].LocalName == src_trg_choice)
                                {

                                    for (int k = 0; k < trans_units[tu_id.Id].ChildNodes[j].ChildNodes.Count; k++)
                                    {
                                        string xml_current = trans_units[tu_id.Id].ChildNodes[j].ChildNodes[k].InnerText;
                                        //xml_current = xml_current.Replace("(", "{");
                                        //xml_current = xml_current.Replace(")", "}");
                                        string to_replace = "displaytext=\"\\{\\{#_\\}\\}" + gID + "\\{\\{\\/_\\}\\}\"";
                                        string xml_updated = Regex.Replace(xml_current, "displaytext=\"\\{\\{#_\\}\\}" + gID + "\\{\\{\\/_\\}\\}\"", $"displaytext=\"{rValue.Replace("\n", " ")}\"");

                                        //Debug.WriteLine($"(xml_updated) {xml_updated}");


                                        if (xml_current != xml_updated)
                                        {
                                            Debug.WriteLine($"Updating src at node {tu_id.Id} {rValue}");
                                            Debug.WriteLine($"(updated) to update {to_replace} :: {xml_updated}");
                                            richTextConsoleBox1.AppendText($"Original:\n {xml_current}\nUpdated:\n{xml_updated}\n\n");
                                            trans_units[tu_id.Id].ChildNodes[j].ChildNodes[k].InnerText = xml_updated;
                                        }

                                    }

                                }

                            }




                        }// end for loop





                    }// end foreach




                }

                xliff.Save(mqxliff.Replace(".mqxliff", "_temp.mqxliff"));

                string temp_file = mqxliff.Replace(".mqxliff", "_temp.mqxliff");
                string all_text = File.ReadAllText(temp_file);

                all_text = Regex.Replace(all_text, "^([ ]+)(?=<)", "", RegexOptions.Multiline);

                all_text = Regex.Replace(all_text, " Idx=\"(\\d+)\"", " Idx=$1");

                File.WriteAllText(temp_file.Replace("_temp.mqxliff", "_updated.mqxliff"), all_text);
                File.Delete(temp_file);

            }


            Debug.WriteLine("Done!");
            richTextConsoleBox1.AppendText($"\nDone!");
        }

    }
}
