﻿namespace DeNA_Prep_and_Finalize
{
    public class Message_Data
    {
        private string id;
        private string jp;
        private string en;
        private string fr;
        private string it;
        private string de;
        private string es;
        private string kr;
        private string tw;

        public Message_Data(string id, string jp, string en, string fr, string it, string de, string es, string kr, string tw)
        {
            this.id = id;
            this.jp = jp;
            this.en = en;
            this.fr = fr;
            this.it = it;
            this.de = de;
            this.es = es;
            this.kr = kr;
            this.tw = tw;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Jp
        {
            get { return jp; }
            set { jp = value; }
        }

        public string En
        {
            get { return en; }
            set { en = value; }
        }

        public string Fr
        {
            get { return fr; }
            set { fr = value; }
        }

        public string It
        {
            get { return it; }
            set { it = value; }
        }

        public string De
        {
            get { return de; }
            set { de = value; }
        }

        public string Es
        {
            get { return es; }
            set { es = value; }
        }

        public string Kr
        {
            get { return kr; }
            set { kr = value; }
        }

        public string Tw
        {
            get { return tw; }
            set { tw = value; }
        }
    }

}
