﻿namespace DeNA_Prep_and_Finalize
{
    partial class FinalizePunctuation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.FixPunctButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.richTextConsoleBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(12, 55);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(776, 145);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // FixPunctButton
            // 
            this.FixPunctButton.Location = new System.Drawing.Point(310, 206);
            this.FixPunctButton.Name = "FixPunctButton";
            this.FixPunctButton.Size = new System.Drawing.Size(174, 43);
            this.FixPunctButton.TabIndex = 1;
            this.FixPunctButton.Text = "Fix Punctuation";
            this.FixPunctButton.UseVisualStyleBackColor = true;
            this.FixPunctButton.Click += new System.EventHandler(this.FixPunctButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select tabs to fix";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(310, 512);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(174, 43);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // richTextConsoleBox1
            // 
            this.richTextConsoleBox1.Location = new System.Drawing.Point(12, 278);
            this.richTextConsoleBox1.Name = "richTextConsoleBox1";
            this.richTextConsoleBox1.Size = new System.Drawing.Size(775, 228);
            this.richTextConsoleBox1.TabIndex = 4;
            this.richTextConsoleBox1.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Output";
            // 
            // FinalizePunctuation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 567);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.richTextConsoleBox1);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FixPunctButton);
            this.Controls.Add(this.listView1);
            this.Name = "FinalizePunctuation";
            this.Text = "FinalizePunctuation";
            this.Load += new System.EventHandler(this.FinalizePunctuation_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button FixPunctButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.RichTextBox richTextConsoleBox1;
        private System.Windows.Forms.Label label2;
    }
}