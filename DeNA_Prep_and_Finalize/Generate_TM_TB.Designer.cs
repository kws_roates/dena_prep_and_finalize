﻿namespace DeNA_Prep_and_Finalize
{
    partial class Generate_TM_TB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseMsg = new System.Windows.Forms.Button();
            this.richTextConsoleBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // CloseMsg
            // 
            this.CloseMsg.Location = new System.Drawing.Point(529, 558);
            this.CloseMsg.Name = "CloseMsg";
            this.CloseMsg.Size = new System.Drawing.Size(221, 37);
            this.CloseMsg.TabIndex = 1;
            this.CloseMsg.Text = "Close";
            this.CloseMsg.UseVisualStyleBackColor = true;
            this.CloseMsg.Click += new System.EventHandler(this.CloseMsg_Click);
            // 
            // richTextConsoleBox1
            // 
            this.richTextConsoleBox1.Location = new System.Drawing.Point(20, 15);
            this.richTextConsoleBox1.Name = "richTextConsoleBox1";
            this.richTextConsoleBox1.Size = new System.Drawing.Size(1215, 523);
            this.richTextConsoleBox1.TabIndex = 2;
            this.richTextConsoleBox1.Text = "";
            this.richTextConsoleBox1.TextChanged += new System.EventHandler(this.richTextConsoleBox1_TextChanged);
            // 
            // ConsoleMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 607);
            this.Controls.Add(this.richTextConsoleBox1);
            this.Controls.Add(this.CloseMsg);
            this.Name = "ConsoleMessages";
            this.Text = "ConsoleMessages";
            this.Load += new System.EventHandler(this.Generate_TM_TB_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button CloseMsg;
        private System.Windows.Forms.RichTextBox richTextConsoleBox1;
    }
}