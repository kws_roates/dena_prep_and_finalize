﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DeNA_Prep_and_Finalize
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Select_TM_TB_Path_Click(object sender, EventArgs e)
        {

            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                // Always default to Folder Selection.
                folderBrowser.FileName = "Folder Selection.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    TM_TB_Path.Text = folderPath;
                }
            }

        }

        private void Select_Reference_Files_Path_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                // Always default to Folder Selection.
                folderBrowser.FileName = "Folder Selection.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    Ref_Folder_Path.Text = folderPath;
                }
            }
        }

        private void Select_MQXLIFF_Path_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                // Always default to Folder Selection.
                folderBrowser.FileName = "Folder Selection.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    Mqxliff_Path.Text = folderPath;
                }
            }

        }

        private void Select_File_to_Finalize_Path_Click(object sender, EventArgs e)
        {
            /*
            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                // Always default to Folder Selection.
                folderBrowser.FileName = "Folder Selection.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    Finalized_Files_Path.Text = folderPath;
                }
            }
            */

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.xlsx;*.xlsm)|*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Finalized_Files_Path.Text = openFileDialog.FileName;
                }
            }


        }

        private void Process_Stuff_Click(object sender, EventArgs e)
        {


            if (Prep_tm_and_tb.Checked == true)
            {
                Debug.WriteLine("Prepping TM and TB");

                if (TM_TB_Path.Text != "")
                {
                    Generate_TM_TB generate_TM_TB = new Generate_TM_TB(TM_TB_Path.Text);
                    generate_TM_TB.Show();
                    generate_TM_TB.Run();
                }
                else
                {
                    MessageBox.Show("Please select a folder!");
                }

            }
            if (ConvertIDxtoRef.Checked == true)
            {
                Debug.WriteLine("Converting IDx");

                if(Ref_Folder_Path.Text!="" && Mqxliff_Path.Text != "")
                {
                    if (convertOption1.Checked)
                    {
                        IDtoReference generate_TM_TB = new IDtoReference(Ref_Folder_Path.Text, Mqxliff_Path.Text, "source");
                        generate_TM_TB.Show();
                        generate_TM_TB.Run();
                    }
                    if (convertOption2.Checked)
                    {
                        IDtoReference generate_TM_TB = new IDtoReference(Ref_Folder_Path.Text, Mqxliff_Path.Text, "target");
                        generate_TM_TB.Show();
                        generate_TM_TB.Run();
                    }

                }
                else
                {
                    MessageBox.Show("Please select a folder!");
                }



            }
            if (FinalizePunctuation.Checked == true)
            {
                Debug.WriteLine("Finalizing files");

                bool check_jp_eck = true;

                if (SelectedLanguage_J_ECK.Checked == false)
                {
                    check_jp_eck = false;
                }

                if (Finalized_Files_Path.Text!="")
                {
                    FinalizePunctuation finalizePunctuation = new FinalizePunctuation(Finalized_Files_Path.Text, check_jp_eck);
                    finalizePunctuation.Run();
                    finalizePunctuation.Show();
                }
                else
                {
                    MessageBox.Show("Please select a file!");
                }
                

            }
        }





        private void Prep_tm_and_tb_CheckedChanged(object sender, EventArgs e)
        {

            Select_TM_TB_Path.Enabled = true;
            TM_TB_Path.Enabled = true;


            //disable everything else when checked
            Select_Reference_Files_Path.Enabled = false;
            Select_MQXLIFF_Path.Enabled = false;
            Ref_Folder_Path.Enabled = false;
            Mqxliff_Path.Enabled = false;

            Select_File_to_Finalize_Path.Enabled = false;
            Finalized_Files_Path.Enabled = false;
            ProjectGroupBox.Enabled = false;



        }

        private void ConvertIDxtoRef_CheckedChanged(object sender, EventArgs e)
        {
            Select_TM_TB_Path.Enabled = false;
            TM_TB_Path.Enabled = false;


            //disable everything else when checked
            Select_Reference_Files_Path.Enabled = true;
            Select_MQXLIFF_Path.Enabled = true;
            Ref_Folder_Path.Enabled = true;
            Mqxliff_Path.Enabled = true;

            Select_File_to_Finalize_Path.Enabled = false;
            Finalized_Files_Path.Enabled = false;
            ProjectGroupBox.Enabled = false;
        }

        private void FinalizePunctuation_CheckedChanged(object sender, EventArgs e)
        {
            Select_TM_TB_Path.Enabled = false;
            TM_TB_Path.Enabled = false;

            //disable everything else when checked
            Select_Reference_Files_Path.Enabled = false;
            Select_MQXLIFF_Path.Enabled = false;
            Ref_Folder_Path.Enabled = false;
            Mqxliff_Path.Enabled = false;

            Select_File_to_Finalize_Path.Enabled = true;
            Finalized_Files_Path.Enabled = true;
            ProjectGroupBox.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SelectedLanguage_J_ECK.Checked = true;
        }

        ///Controller



        private void TM_TB_Path_TextChanged(object sender, EventArgs e)
        {

        }
    }

}
