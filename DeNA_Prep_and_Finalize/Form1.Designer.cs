﻿namespace DeNA_Prep_and_Finalize
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Prep_tm_and_tb = new System.Windows.Forms.RadioButton();
            this.ConvertIDxtoRef = new System.Windows.Forms.RadioButton();
            this.FinalizePunctuation = new System.Windows.Forms.RadioButton();
            this.Process_Stuff = new System.Windows.Forms.Button();
            this.Select_TM_TB_Path = new System.Windows.Forms.Button();
            this.Select_Reference_Files_Path = new System.Windows.Forms.Button();
            this.Select_MQXLIFF_Path = new System.Windows.Forms.Button();
            this.Select_File_to_Finalize_Path = new System.Windows.Forms.Button();
            this.TM_TB_Path = new System.Windows.Forms.TextBox();
            this.Ref_Folder_Path = new System.Windows.Forms.TextBox();
            this.Mqxliff_Path = new System.Windows.Forms.TextBox();
            this.Finalized_Files_Path = new System.Windows.Forms.TextBox();
            this.ProjectGroupBox = new System.Windows.Forms.GroupBox();
            this.SelectedLanguage_E_FIGS = new System.Windows.Forms.RadioButton();
            this.SelectedLanguage_J_ECK = new System.Windows.Forms.RadioButton();
            this.convertOptGroup = new System.Windows.Forms.GroupBox();
            this.convertOption2 = new System.Windows.Forms.RadioButton();
            this.convertOption1 = new System.Windows.Forms.RadioButton();
            this.ProjectGroupBox.SuspendLayout();
            this.convertOptGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // Prep_tm_and_tb
            // 
            this.Prep_tm_and_tb.AutoSize = true;
            this.Prep_tm_and_tb.Location = new System.Drawing.Point(47, 57);
            this.Prep_tm_and_tb.Name = "Prep_tm_and_tb";
            this.Prep_tm_and_tb.Size = new System.Drawing.Size(225, 21);
            this.Prep_tm_and_tb.TabIndex = 0;
            this.Prep_tm_and_tb.TabStop = true;
            this.Prep_tm_and_tb.Text = "Prepare TM and TBs for Import";
            this.Prep_tm_and_tb.UseVisualStyleBackColor = true;
            this.Prep_tm_and_tb.CheckedChanged += new System.EventHandler(this.Prep_tm_and_tb_CheckedChanged);
            // 
            // ConvertIDxtoRef
            // 
            this.ConvertIDxtoRef.AutoSize = true;
            this.ConvertIDxtoRef.Location = new System.Drawing.Point(47, 167);
            this.ConvertIDxtoRef.Name = "ConvertIDxtoRef";
            this.ConvertIDxtoRef.Size = new System.Drawing.Size(195, 21);
            this.ConvertIDxtoRef.TabIndex = 1;
            this.ConvertIDxtoRef.TabStop = true;
            this.ConvertIDxtoRef.Text = "Convert ID_x to Reference";
            this.ConvertIDxtoRef.UseVisualStyleBackColor = true;
            this.ConvertIDxtoRef.CheckedChanged += new System.EventHandler(this.ConvertIDxtoRef_CheckedChanged);
            // 
            // FinalizePunctuation
            // 
            this.FinalizePunctuation.AutoSize = true;
            this.FinalizePunctuation.Location = new System.Drawing.Point(47, 321);
            this.FinalizePunctuation.Name = "FinalizePunctuation";
            this.FinalizePunctuation.Size = new System.Drawing.Size(200, 21);
            this.FinalizePunctuation.TabIndex = 2;
            this.FinalizePunctuation.TabStop = true;
            this.FinalizePunctuation.Text = "Finalize Punctuation in files";
            this.FinalizePunctuation.UseVisualStyleBackColor = true;
            this.FinalizePunctuation.CheckedChanged += new System.EventHandler(this.FinalizePunctuation_CheckedChanged);
            // 
            // Process_Stuff
            // 
            this.Process_Stuff.Location = new System.Drawing.Point(365, 441);
            this.Process_Stuff.Name = "Process_Stuff";
            this.Process_Stuff.Size = new System.Drawing.Size(208, 55);
            this.Process_Stuff.TabIndex = 3;
            this.Process_Stuff.Text = "Process";
            this.Process_Stuff.UseVisualStyleBackColor = true;
            this.Process_Stuff.Click += new System.EventHandler(this.Process_Stuff_Click);
            // 
            // Select_TM_TB_Path
            // 
            this.Select_TM_TB_Path.Location = new System.Drawing.Point(47, 84);
            this.Select_TM_TB_Path.Name = "Select_TM_TB_Path";
            this.Select_TM_TB_Path.Size = new System.Drawing.Size(212, 33);
            this.Select_TM_TB_Path.TabIndex = 4;
            this.Select_TM_TB_Path.Text = "Select folder";
            this.Select_TM_TB_Path.UseVisualStyleBackColor = true;
            this.Select_TM_TB_Path.Click += new System.EventHandler(this.Select_TM_TB_Path_Click);
            // 
            // Select_Reference_Files_Path
            // 
            this.Select_Reference_Files_Path.Location = new System.Drawing.Point(47, 194);
            this.Select_Reference_Files_Path.Name = "Select_Reference_Files_Path";
            this.Select_Reference_Files_Path.Size = new System.Drawing.Size(213, 33);
            this.Select_Reference_Files_Path.TabIndex = 5;
            this.Select_Reference_Files_Path.Text = "Select folder to reference files";
            this.Select_Reference_Files_Path.UseVisualStyleBackColor = true;
            this.Select_Reference_Files_Path.Click += new System.EventHandler(this.Select_Reference_Files_Path_Click);
            // 
            // Select_MQXLIFF_Path
            // 
            this.Select_MQXLIFF_Path.Location = new System.Drawing.Point(47, 233);
            this.Select_MQXLIFF_Path.Name = "Select_MQXLIFF_Path";
            this.Select_MQXLIFF_Path.Size = new System.Drawing.Size(212, 34);
            this.Select_MQXLIFF_Path.TabIndex = 6;
            this.Select_MQXLIFF_Path.Text = "Select folder to mqxliffs";
            this.Select_MQXLIFF_Path.UseVisualStyleBackColor = true;
            this.Select_MQXLIFF_Path.Click += new System.EventHandler(this.Select_MQXLIFF_Path_Click);
            // 
            // Select_File_to_Finalize_Path
            // 
            this.Select_File_to_Finalize_Path.Location = new System.Drawing.Point(47, 348);
            this.Select_File_to_Finalize_Path.Name = "Select_File_to_Finalize_Path";
            this.Select_File_to_Finalize_Path.Size = new System.Drawing.Size(213, 34);
            this.Select_File_to_Finalize_Path.TabIndex = 7;
            this.Select_File_to_Finalize_Path.Text = "Select file to finalize";
            this.Select_File_to_Finalize_Path.UseVisualStyleBackColor = true;
            this.Select_File_to_Finalize_Path.Click += new System.EventHandler(this.Select_File_to_Finalize_Path_Click);
            // 
            // TM_TB_Path
            // 
            this.TM_TB_Path.Location = new System.Drawing.Point(322, 89);
            this.TM_TB_Path.Name = "TM_TB_Path";
            this.TM_TB_Path.Size = new System.Drawing.Size(403, 22);
            this.TM_TB_Path.TabIndex = 8;
            this.TM_TB_Path.TextChanged += new System.EventHandler(this.TM_TB_Path_TextChanged);
            // 
            // Ref_Folder_Path
            // 
            this.Ref_Folder_Path.Location = new System.Drawing.Point(322, 199);
            this.Ref_Folder_Path.Name = "Ref_Folder_Path";
            this.Ref_Folder_Path.Size = new System.Drawing.Size(401, 22);
            this.Ref_Folder_Path.TabIndex = 9;
            // 
            // Mqxliff_Path
            // 
            this.Mqxliff_Path.Location = new System.Drawing.Point(324, 239);
            this.Mqxliff_Path.Name = "Mqxliff_Path";
            this.Mqxliff_Path.Size = new System.Drawing.Size(399, 22);
            this.Mqxliff_Path.TabIndex = 10;
            // 
            // Finalized_Files_Path
            // 
            this.Finalized_Files_Path.Location = new System.Drawing.Point(325, 354);
            this.Finalized_Files_Path.Name = "Finalized_Files_Path";
            this.Finalized_Files_Path.Size = new System.Drawing.Size(398, 22);
            this.Finalized_Files_Path.TabIndex = 11;
            // 
            // ProjectGroupBox
            // 
            this.ProjectGroupBox.Controls.Add(this.SelectedLanguage_E_FIGS);
            this.ProjectGroupBox.Controls.Add(this.SelectedLanguage_J_ECK);
            this.ProjectGroupBox.Location = new System.Drawing.Point(47, 388);
            this.ProjectGroupBox.Name = "ProjectGroupBox";
            this.ProjectGroupBox.Size = new System.Drawing.Size(172, 109);
            this.ProjectGroupBox.TabIndex = 12;
            this.ProjectGroupBox.TabStop = false;
            this.ProjectGroupBox.Text = "Project";
            // 
            // SelectedLanguage_E_FIGS
            // 
            this.SelectedLanguage_E_FIGS.AutoSize = true;
            this.SelectedLanguage_E_FIGS.Location = new System.Drawing.Point(24, 65);
            this.SelectedLanguage_E_FIGS.Name = "SelectedLanguage_E_FIGS";
            this.SelectedLanguage_E_FIGS.Size = new System.Drawing.Size(92, 21);
            this.SelectedLanguage_E_FIGS.TabIndex = 1;
            this.SelectedLanguage_E_FIGS.TabStop = true;
            this.SelectedLanguage_E_FIGS.Text = "EN->FIGS";
            this.SelectedLanguage_E_FIGS.UseVisualStyleBackColor = true;
            // 
            // SelectedLanguage_J_ECK
            // 
            this.SelectedLanguage_J_ECK.AutoSize = true;
            this.SelectedLanguage_J_ECK.Location = new System.Drawing.Point(24, 38);
            this.SelectedLanguage_J_ECK.Name = "SelectedLanguage_J_ECK";
            this.SelectedLanguage_J_ECK.Size = new System.Drawing.Size(85, 21);
            this.SelectedLanguage_J_ECK.TabIndex = 0;
            this.SelectedLanguage_J_ECK.TabStop = true;
            this.SelectedLanguage_J_ECK.Text = "JP->ECK";
            this.SelectedLanguage_J_ECK.UseVisualStyleBackColor = true;
            // 
            // convertOptGroup
            // 
            this.convertOptGroup.Controls.Add(this.convertOption2);
            this.convertOptGroup.Controls.Add(this.convertOption1);
            this.convertOptGroup.Location = new System.Drawing.Point(750, 167);
            this.convertOptGroup.Name = "convertOptGroup";
            this.convertOptGroup.Size = new System.Drawing.Size(201, 119);
            this.convertOptGroup.TabIndex = 13;
            this.convertOptGroup.TabStop = false;
            this.convertOptGroup.Text = "Convert";
            // 
            // convertOption2
            // 
            this.convertOption2.AutoSize = true;
            this.convertOption2.Location = new System.Drawing.Point(34, 72);
            this.convertOption2.Name = "convertOption2";
            this.convertOption2.Size = new System.Drawing.Size(71, 21);
            this.convertOption2.TabIndex = 1;
            this.convertOption2.Text = "Target";
            this.convertOption2.UseVisualStyleBackColor = true;
            // 
            // convertOption1
            // 
            this.convertOption1.AutoSize = true;
            this.convertOption1.Checked = true;
            this.convertOption1.Location = new System.Drawing.Point(34, 39);
            this.convertOption1.Name = "convertOption1";
            this.convertOption1.Size = new System.Drawing.Size(74, 21);
            this.convertOption1.TabIndex = 0;
            this.convertOption1.TabStop = true;
            this.convertOption1.Text = "Source";
            this.convertOption1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 532);
            this.Controls.Add(this.convertOptGroup);
            this.Controls.Add(this.ProjectGroupBox);
            this.Controls.Add(this.Finalized_Files_Path);
            this.Controls.Add(this.Mqxliff_Path);
            this.Controls.Add(this.Ref_Folder_Path);
            this.Controls.Add(this.TM_TB_Path);
            this.Controls.Add(this.Select_File_to_Finalize_Path);
            this.Controls.Add(this.Select_MQXLIFF_Path);
            this.Controls.Add(this.Select_Reference_Files_Path);
            this.Controls.Add(this.Select_TM_TB_Path);
            this.Controls.Add(this.Process_Stuff);
            this.Controls.Add(this.FinalizePunctuation);
            this.Controls.Add(this.ConvertIDxtoRef);
            this.Controls.Add(this.Prep_tm_and_tb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "DeNA Prep and Finalize";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ProjectGroupBox.ResumeLayout(false);
            this.ProjectGroupBox.PerformLayout();
            this.convertOptGroup.ResumeLayout(false);
            this.convertOptGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Prep_tm_and_tb;
        private System.Windows.Forms.RadioButton ConvertIDxtoRef;
        private System.Windows.Forms.RadioButton FinalizePunctuation;
        private System.Windows.Forms.Button Process_Stuff;
        private System.Windows.Forms.Button Select_TM_TB_Path;
        private System.Windows.Forms.Button Select_Reference_Files_Path;
        private System.Windows.Forms.Button Select_MQXLIFF_Path;
        private System.Windows.Forms.Button Select_File_to_Finalize_Path;
        private System.Windows.Forms.TextBox TM_TB_Path;
        private System.Windows.Forms.TextBox Ref_Folder_Path;
        private System.Windows.Forms.TextBox Mqxliff_Path;
        private System.Windows.Forms.TextBox Finalized_Files_Path;
        private System.Windows.Forms.GroupBox ProjectGroupBox;
        private System.Windows.Forms.RadioButton SelectedLanguage_E_FIGS;
        private System.Windows.Forms.RadioButton SelectedLanguage_J_ECK;
        private System.Windows.Forms.GroupBox convertOptGroup;
        private System.Windows.Forms.RadioButton convertOption2;
        private System.Windows.Forms.RadioButton convertOption1;
    }
}

