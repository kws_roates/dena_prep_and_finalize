﻿namespace DeNA_Prep_and_Finalize
{
    public class TransUnit
    {
        private int id;
        private string src;
        private string note;

        public TransUnit(int id, string src, string note)
        {

            this.id = id;
            this.src = src;
            this.note = note;

        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Src
        {
            get { return src; }
            set { src = value; }
        }

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
