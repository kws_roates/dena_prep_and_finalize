﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeNA_Prep_and_Finalize
{
    public partial class FinalizePunctuation : Form
    {
        private string Finalized_Files_Path;
        private bool j_eck;

        public FinalizePunctuation(string Finalized_Files_Path, bool check_jp_eck)
        {
            InitializeComponent();
            this.Finalized_Files_Path = Finalized_Files_Path;
            j_eck = check_jp_eck;

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
        }

        private void FinalizePunctuation_Load(object sender, EventArgs e)
        {

        }

        public void Run()
        {
            ExcelPackage wb = new ExcelPackage(new System.IO.FileInfo(Finalized_Files_Path));
            listView1.Items.Clear();

            foreach (ExcelWorksheet ws in wb.Workbook.Worksheets)
            {
                if (ws.Hidden.ToString() == "Visible")
                {
                    if (ws.Name.StartsWith("Announcements") || ws.Name.StartsWith("Tips") || ws.Name.StartsWith("Sheet1") || ws.Name.StartsWith("Articles"))
                    {
                        listView1.Items.Add(ws.Name).Checked = true;
                    }
                    else
                    {
                        listView1.Items.Add(ws.Name).Checked = false;
                    }

                }
            }
        }

        private void FinalizePunctuation_routine()
        {


            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            int cantfind = -1;


            string filename = Finalized_Files_Path;

            filename = filename.Replace("\"", "");



            ExcelPackage wb = new ExcelPackage(new FileInfo(filename));

            foreach (ListViewItem sheet in listView1.CheckedItems)
            {

                ExcelWorksheet ws = wb.Workbook.Worksheets[sheet.Text];


                int id = cantfind;
                int jp = cantfind;
                int en = cantfind;
                int fr = cantfind;
                int it = cantfind;
                int de = cantfind;
                int es = cantfind;
                int kr = cantfind;
                int tw = cantfind;
                int ms_comment = cantfind;




                for (int header = 1; header < ws.Dimension.Columns + 1; header++)
                {
                    /* find the header */
                    if (ws.Cells[1, header].Value != null)
                    {
                        if (ws.Cells[1, header].Value.ToString() == "ja")
                        {
                            jp = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "String ID")
                        {
                            id = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "En_kw")
                        {
                            en = header;
                        }

                        if (ws.Cells[1, header].Value.ToString() == "Fr_kw")
                        {
                            fr = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "It_kw")
                        {
                            it = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "De_kw")
                        {
                            de = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "Es_kw")
                        {
                            es = header;
                        }
                        if (ws.Cells[1, header].Value.ToString() == "ＭＳコメント")
                        {
                            ms_comment = header;
                        }
                    }

                }
                Console.WriteLine($"{jp}");
                for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                {
                    if (jp > 0)
                    {
                        if (ws.Cells[row, jp].Value != null)
                        {
                            string original_segment = ws.Cells[row, jp].Value.ToString();
                            string cleaned_segment = Regex.Replace(original_segment, "\r", "");
                            ws.Cells[row, jp].Value = cleaned_segment;
                        }


                    }


                    if (en > 0)
                    {
                        if (ws.Cells[row, en].Value != null)
                        {


                            string original_segment = ws.Cells[row, en].Value.ToString();
                            string cleaned_segment = Regex.Replace(original_segment, "\r", "");

                            if (j_eck == true)
                            {
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)((&nbsp;)?[!,\.:;\?])", "$2$1");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)(s)(?= |<|$|\r?\n)", "$2$1");
                                //move l' or d' inside a <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "([lLdD]\')(<span class=\"word.*?\">)", "$2$1");
                                //Add a space
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?![ >])(.)(<span class=\"word.*?\">)", "$1 $2");
                                //Add a space after the </span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(</span>)(?![ <]|&nbsp;|\r?\n|\\*|\\))(.)", "$1 $2");
                                //subtract spaces between &nbsp; <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(&nbsp;) (<span class=\"word.*?\">)", "$1$2");

                                //move puntuations back to the ouside again :p
                                cleaned_segment = Regex.Replace(cleaned_segment, "(<span class=\"word em\">)(.*?)((&nbsp;)?[!,\\.:;\\?])(</span>)", "$1$2$5$3$4");

                                //Clean double spaces
                                cleaned_segment = Regex.Replace(cleaned_segment, "(  )", " ");

                                if (original_segment != cleaned_segment)
                                {
                                    richTextConsoleBox1.AppendText($"[English] Before:\n{original_segment}\n[English] After:\n" +
                                        $"{cleaned_segment}\n\n");
                                }
                            }

                            ws.Cells[row, en].Value = cleaned_segment;
                        }
                    }


                    if (j_eck == false)
                    {
                        if (fr > 0)
                        {
                            if (ws.Cells[row, fr].Value != null)
                            {


                                string original_segment = ws.Cells[row, fr].Value.ToString();
                                string cleaned_segment = Regex.Replace(original_segment, "\r", "");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)((&nbsp;)?[!,\.:;\?])", "$2$1");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)(s)(?= |<|$|\r?\n)", "$2$1");
                                //move l' or d' inside a <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "([lLdD]\')(<span class=\"word.*?\">)", "$2$1");
                                //Add a space
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?![ >])(.)(<span class=\"word.*?\">)", "$1 $2");
                                //Add a space after the </span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(</span>)(?![ <]|&nbsp;|\r?\n|\\*|\\))(.)", "$1 $2");
                                //subtract spaces between &nbsp; <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(&nbsp;) (<span class=\"word.*?\">)", "$1$2");


                                //move puntuations back to the ouside again :p
                                cleaned_segment = Regex.Replace(cleaned_segment, "(<span class=\"word em\">)(.*?)((&nbsp;)?[!,\\.:;\\?])(</span>)", "$1$2$5$3$4");

                                //Clean double spaces
                                cleaned_segment = Regex.Replace(cleaned_segment, "(  )", " ");

                                if (ms_comment > 0)
                                {
                                    if (ws.Cells[row, ms_comment].Value != null)
                                    {
                                        cleaned_segment = AddBoldTags(ws.Cells[row, ms_comment].Value.ToString(), cleaned_segment);
                                    }
                                }
                                ws.Cells[row, fr].Value = cleaned_segment;
                                if (original_segment != cleaned_segment)
                                {
                                    richTextConsoleBox1.AppendText($"[French] Before:\n{original_segment}\n[French] After:\n" +
                                        $"{cleaned_segment}\n\n");
                                }
                            }
                        }

                        if (it > 0)
                        {
                            if (ws.Cells[row, it].Value != null)
                            {


                                string original_segment = ws.Cells[row, it].Value.ToString();
                                string cleaned_segment = Regex.Replace(original_segment, "\r", "");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)((&nbsp;)?[!,\.:;\?])", "$2$1");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)(s)(?= |<|$|\r?\n)", "$2$1");
                                //move l' or d' inside a <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "([lLdD]\')(<span class=\"word.*?\">)", "$2$1");
                                //Add a space
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?![ >])(.)(<span class=\"word.*?\">)", "$1 $2");
                                //Add a space after the </span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(</span>)(?![ <]|&nbsp;|\r?\n|\\*|\\))(.)", "$1 $2");
                                //subtract spaces between &nbsp; <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(&nbsp;) (<span class=\"word.*?\">)", "$1$2");

                                //move puntuations back to the ouside again :p
                                cleaned_segment = Regex.Replace(cleaned_segment, "(<span class=\"word em\">)(.*?)((&nbsp;)?[!,\\.:;\\?])(</span>)", "$1$2$5$3$4");

                                //Clean double spaces
                                cleaned_segment = Regex.Replace(cleaned_segment, "(  )", " ");

                                if (ms_comment > 0)
                                {
                                    if (ws.Cells[row, ms_comment].Value != null)
                                    {
                                        cleaned_segment = AddBoldTags(ws.Cells[row, ms_comment].Value.ToString(), cleaned_segment);
                                    }
                                }
                                ws.Cells[row, it].Value = cleaned_segment;
                                if (original_segment != cleaned_segment)
                                {
                                    richTextConsoleBox1.AppendText($"[Italian] Before:\n{original_segment}\n[Italian] After:\n" +
                                        $"{cleaned_segment}\n\n");
                                }
                            }
                        }

                        if (de > 0)
                        {
                            if (ws.Cells[row, de].Value!=null)
                            {
                                //German basically requires us to move hyphens inside
                                string original_segment = ws.Cells[row, de].Value.ToString();
                                string cleaned_segment = Regex.Replace(original_segment, "\r", "");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)([\-!,\.:;\?])", "$2$1");
                                //move - inside (not needed)
                                //cleaned_segment = Regex.Replace(cleaned_segment, "(-)(<span class=\"word.*?\">)", "$2$1");
                                //add a space if <span>, not <span>-
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?![ >\\-])(.)(<span class=\"word.*?\">(?!-))", "$1 $2");
                                //Add a space after the </span>, not  -</span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?!-)(.</span>)(?![ <]|&nbsp;|\r?\n|\\*|\\))(.)", "$1 $2");
                                //subtract spaces between &nbsp; <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(&nbsp;) (<span class=\"word.*?\">)", "$1$2");

                                //move puntuations back to the ouside again :p
                                cleaned_segment = Regex.Replace(cleaned_segment, "(<span class=\"word em\">)(.*?)([!,\\.:;\\?])(</span>)", "$1$2$4$3");

                                //Clean double spaces
                                cleaned_segment = Regex.Replace(cleaned_segment, "(  )", " ");

                                if (ms_comment > 0)
                                {
                                    if (ws.Cells[row, ms_comment].Value != null)
                                    {
                                        cleaned_segment = AddBoldTags(ws.Cells[row, ms_comment].Value.ToString(), cleaned_segment);
                                    }
                                }
                                ws.Cells[row, de].Value = cleaned_segment;
                                if (original_segment != cleaned_segment)
                                {
                                    richTextConsoleBox1.AppendText($"[German] Before:\n{original_segment}\n[German] After:\n" +
                                        $"{cleaned_segment}\n\n");
                                }
                            }
                            
                        }

                        if (es > 0)
                        {
                            if(ws.Cells[row, es].Value != null)
                            {
                                string original_segment = ws.Cells[row, es].Value.ToString();
                                string cleaned_segment = Regex.Replace(original_segment, "\r", "");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)([!,\.:;\?])", "$2$1");
                                cleaned_segment = Regex.Replace(cleaned_segment, @"(</span>)(s)(?= |<|$|\r?\n)", "$2$1");
                                //move ¿ or ¡ inside a <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(¿|¡)(<span class=\"word.*?\">)", "$2$1");
                                //Add a space
                                cleaned_segment = Regex.Replace(cleaned_segment, "(?![ >])(.)(<span class=\"word.*?\">)", "$1 $2");
                                //Add a space after the </span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(</span>)(?![ <]|&nbsp;|\r?\n|\\*|\\))(.)", "$1 $2");
                                //subtract spaces between &nbsp; <span>
                                cleaned_segment = Regex.Replace(cleaned_segment, "(&nbsp;) (<span class=\"word.*?\">)", "$1$2");

                                //move puntuations back to the ouside again :p
                                cleaned_segment = Regex.Replace(cleaned_segment, "(<span class=\"word em\">)(.*?)([!,\\.:;\\?])(</span>)", "$1$2$4$3");

                                //Clean double spaces
                                cleaned_segment = Regex.Replace(cleaned_segment, "(  )", " ");

                                if (ms_comment > 0)
                                {
                                    if (ws.Cells[row, ms_comment].Value != null)
                                    {
                                        cleaned_segment = AddBoldTags(ws.Cells[row, ms_comment].Value.ToString(), cleaned_segment);
                                    }
                                }
                                ws.Cells[row, es].Value = cleaned_segment;
                                if (original_segment != cleaned_segment)
                                {
                                    richTextConsoleBox1.AppendText($"[Spanish] Before:\n{original_segment}\n[Spanish] After:\n" +
                                        $"{cleaned_segment}\n\n");
                                }
                            }
                            
                        }

                    }

                }

            }
            wb.SaveAs(new FileInfo(filename.Replace(".xlsx", "_tag_fixed.xlsx")));


            Debug.WriteLine("DONE");
            richTextConsoleBox1.AppendText("Done\n\n");

        }

        private string AddBoldTags(string ms_comment, string figs_segment)
        {
            // Add <b> tags if we encounter the following rules
            //
            // banner_text
            // event_name
            // story_quest_name
            // quest_title

            

            string[] words = ms_comment.Split('\n');

            for (int i = 0; i < words.Count(); i++)
            {
                //Debug.WriteLine($"{i} = {words[i]}");
                Match regxmsg_value = Regex.Match(words[i], "(.*)=(.*?)$");
                if (regxmsg_value.Success)
                {
                    string msg_value = regxmsg_value.Groups[2].Value;
                    string msg_key = regxmsg_value.Groups[1].Value;
                    if (msg_value.Contains("banner_text") || msg_value.Contains("event_name") ||
                        msg_value.Contains("story_quest_name") || msg_value.Contains("quest_title"))
                    {
                        //Debug.WriteLine($"{msg_value}=>{msg_key}");
                        figs_segment = figs_segment.Replace(msg_key, $"<b>{msg_key}</b>");

                        figs_segment = figs_segment.Replace("\"<b>", "<b>");
                        figs_segment = figs_segment.Replace("</b>\"", "</b>");

                        //I'm too lazy 
                        figs_segment = figs_segment.Replace("</b>_m!", "_m!</b>");
                        figs_segment = figs_segment.Replace("</b>_u!", "_u!</b>");
                        figs_segment = figs_segment.Replace("</b>_p!", "_p!</b>");
                    }
                    
                }
            }


            return figs_segment;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FixPunctButton_Click(object sender, EventArgs e)
        {
            FinalizePunctuation_routine();
        }
    }
}
