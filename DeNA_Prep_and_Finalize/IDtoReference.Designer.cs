﻿namespace DeNA_Prep_and_Finalize
{
    partial class IDtoReference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextConsoleBox1 = new System.Windows.Forms.RichTextBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextConsoleBox1
            // 
            this.richTextConsoleBox1.Location = new System.Drawing.Point(15, 12);
            this.richTextConsoleBox1.Name = "richTextConsoleBox1";
            this.richTextConsoleBox1.Size = new System.Drawing.Size(1268, 529);
            this.richTextConsoleBox1.TabIndex = 0;
            this.richTextConsoleBox1.Text = "";
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(529, 553);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(243, 38);
            this.CloseButton.TabIndex = 1;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // IDtoReference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1302, 603);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.richTextConsoleBox1);
            this.Name = "IDtoReference";
            this.Text = "IDtoReference";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextConsoleBox1;
        private System.Windows.Forms.Button CloseButton;
    }
}